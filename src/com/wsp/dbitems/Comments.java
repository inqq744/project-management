package com.wsp.dbitems;

public class Comments implements Comparable{
	private Long id;
	private int taskNumber;
	private int commentID;
	private String comment;
	private String author;
	

	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public int getTaskNumber() {
		return taskNumber;
	}
	public void setTaskNumber(int taskNumber) {
		this.taskNumber = taskNumber;
	}
	public int getCommentID() {
		return commentID;
	}
	public void setCommentID(int commentID) {
		this.commentID = commentID;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public int compareTo(Object comm) {
		int commid = ((Comments)comm).getCommentID();
		return this.commentID-commid;
	}
	
}
