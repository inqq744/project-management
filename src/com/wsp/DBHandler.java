package com.wsp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;

import com.wsp.dbitems.Comments;
import com.wsp.dbitems.Groups;
import com.wsp.dbitems.Tasks;
import com.wsp.dbitems.User;
import com.wsp.front.Frame;
import com.wsp.front.Login;
 
 
public class DBHandler {

    public static void main(String[] args) {
		String a="a";
		String b="b";
		DBHandler dbHandler = new DBHandler();
		//dbHandler.querryAllTasks();
		Login login = new Login();
		//Frame frame = new Frame();
		
		dbHandler.querryGroupsForUser("Tom");
    }
    
    //ZWRACA WSZYSTKIE TASKI!
    public List<Tasks> querryAllTasks(){
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
    	Query query = session.createQuery("from Tasks");                 
        List <Tasks>tasksList = query.list();
      
     
        session.getTransaction().commit();
        for(Tasks item : tasksList)
        	System.out.println("Descr:" + item.getDescription() + ", TaskName: " + item.getTaskName());
		return tasksList;
    	
    }
    
    
    //ZWRACA TASKI O WARTOSCI STATUS 'status'  Done/In progress/Pending
    public List<Tasks> querryPrintStatusTasks(String status){
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
    	  Query query = session.createQuery("from Tasks where STATUS='" + status+"'");                 
          List <Tasks>tasksList = query.list();
          java.util.Iterator<Tasks> iter = tasksList.iterator();

          session.getTransaction().commit();
          for(Tasks item : tasksList)
          	System.out.println(item.getDescription() + " " + item.getStatus());
  		return tasksList;
    }
    
    
    //ZWRACA KOLEJNE KOMENTARZE! DLA TASKA O NUMERZE "taskNumber" POSORTOWANE!
    public List<Comments> querryComments(String taskNumber){
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
    	Query query = session.createQuery("from Comments where TASKID = '" + taskNumber + "'");                 
        List <Comments>commentsList = query.list();
        java.util.Iterator<Comments> iter = commentsList.iterator();
        
        session.getTransaction().commit();
        Collections.sort(commentsList);
        for(Comments item : commentsList)
        	System.out.println("nr komentarza:" + item.getCommentID() + ", autor: " + item.getAuthor() + ", komentarz: " +item.getComment());
		return commentsList;
    	
    }
 // ZWRACA WSZYSTKIE GRUPY (BEZ POWTORZEN - do wyświetlenia grup na listach w JPanelach)
    public List<String> querryAllGroups() { 
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from Groups GROUP BY GROUPNAME");                 
        List <Groups>list = query.list();
        java.util.Iterator<Groups> iter = list.iterator();
        List <String> groupsList = new ArrayList<String>();
        while (iter.hasNext()) {
         	Groups group = iter.next();
         	groupsList.add(group.getGroupName());
        }
        session.getTransaction().commit();
        for(String item : groupsList)
        	System.out.println(item);
		return groupsList;
    }
    
    
    public List<String> querryUsersInGroup(String groupName) { 
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from Groups WHERE GROUPNAME ='"+groupName+"'");        
        
        List <Groups>list = query.list();
        java.util.Iterator<Groups> iter = list.iterator();
        List <String> usersList = new ArrayList<String>();
        while (iter.hasNext()) {
         	Groups group = iter.next();
         	usersList.add(group.getUserName());
        }
        session.getTransaction().commit();
        for(String item : usersList)
        	System.out.println(item);
		return usersList;
    }
    
    
    public List<String> querryGroupsForUser(String userName) { 
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from Groups WHERE USERNAME ='"+userName+"'");        
        
        List <Groups>list = query.list();
        java.util.Iterator<Groups> iter = list.iterator();
        List <String> groupsList = new ArrayList<String>();
        while (iter.hasNext()) {
         	Groups group = iter.next();
         	groupsList.add(group.getGroupName());
        }
        session.getTransaction().commit();
        for(String item : groupsList)
        	System.out.println(item);
		return groupsList;
    }
    public List<String> querryAllUsers() { 
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from User GROUP BY USERNAME");                 
        List <User>list = query.list();
        java.util.Iterator<User> iter = list.iterator();
        List <String> usersList = new ArrayList<String>();
        while (iter.hasNext()) {
        	User user = iter.next();
        	usersList.add(user.getUserName());
        }
        session.getTransaction().commit();
        for(String item : usersList)
        	System.out.println(item);
		return usersList;
    }
    
    public List<Groups> querryAllGroupsItems() { 
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from Groups ORDER BY GROUPNAME");                 
        List <Groups>list = query.list();      
        session.getTransaction().commit();
        for(Groups item : list)
        	System.out.println(item.getGroupName() + " " + item.getUserName());
		return list;
    }
    
    public String querryPass(String userName) { 
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from User WHERE USERNAME='"+userName+"'");                 
        List <User>list = query.list();      
        session.getTransaction().commit();
        String pass="/none/";
        if(!list.isEmpty())
        	pass = list.get(0).getPassword();
		return pass;
    }
 // ZWRACA WSZYSTKICH UZYTKOWNIKOW GRUPY PODANEJ W ARGUMENCIE 'groupName'
    public List<String> querryUsersInSpecificGroup(String groupName) {
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from Groups where GROUPNAME = '" + groupName + "'");                 
        List <Groups>list = query.list();
        java.util.Iterator<Groups> iter = list.iterator();
        List <String> usersInGroupList = new ArrayList<String>();
        while (iter.hasNext()) {
         	Groups group = iter.next();
         	usersInGroupList.add(group.getUserName());
        }
        session.getTransaction().commit();
        for(String item : usersInGroupList)
        	System.out.println(item);
		return usersInGroupList;
    }
    //ZWRACA LISTE TASKOW! DLA GRUPY PODANEJ W ARGUMENCIE 'groupName'
    public List<Tasks> querryTasksForSpecificGroup(String groupName) {
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from Tasks where ASSIGNEDTO = '" + groupName + "'");                 
        List <Tasks>tasksList = query.list();
        java.util.Iterator<Tasks> iter = tasksList.iterator();
        while (iter.hasNext()) {
         	Tasks task = iter.next();
        }
        session.getTransaction().commit();
       
		return tasksList;
    }    
    
    public List<Tasks> querryTaskWithID(String id) {
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Query query = session.createQuery("from Tasks where TASKNUMBER = '" + id + "'");                 
        List <Tasks>tasksList = query.list();
        java.util.Iterator<Tasks> iter = tasksList.iterator();
        while (iter.hasNext()) {
         	Tasks task = iter.next();
        }
        session.getTransaction().commit();
       
		return tasksList;
    }  
 
    //TWORZY NOWEGO UZYTKOWNIKA
    public void createUser(String userName, String password) {
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        User user = new User();
 
        user.setUserName(userName);
        user.setPassword(password);         
 
        session.save(user);
    }
    
    // TWORZY NOWA GRUPE
    public void createGroup(String groupName, String userName) {
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
        Groups group = new Groups();
 
        group.setGroupName(groupName);
        group.setUserName(userName);         
 
        session.save(group);
    }
    
    //TWORZY NOWEGO TASKA
    public void createTask(int taskNumber, String taskName, String taskDeadline, String status, String assignedTo, String description) {
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
    	Tasks task = new Tasks();
        
        task.setTaskNumber(taskNumber);
        task.setTaskName(taskName);  
        task.setTaskDeadline(taskDeadline);   
        task.setStatus(status);   
        task.setAssignedTo(assignedTo);   
        task.setDescription(description);   
        
        session.save(task);
    }
    
    //TWORZY KOLEJNY KOMENTARZ UWZGLEDNIAJAC JEGO KOLEJNOSC
    public void createComments(int taskNumber, String comment, String author) {
    	Session session = SessionFactoryUtil.getSessionFactory().getCurrentSession();
    	session.beginTransaction();
    	Criteria cr = session.createCriteria(Comments.class);
    	cr.setProjection(Projections.projectionList()
    	            .add(Projections.max("commentID")));
    	Integer lastComment = (Integer)cr.uniqueResult();

    	Comments comm = new Comments();
    	
    	comm.setTaskNumber(taskNumber);
        comm.setCommentID(lastComment+1);
        comm.setAuthor(author);
        comm.setComment(comment);
        
        session.save(comm);
    }
    
}