package com.wsp.front;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.AbstractListModel;
import javax.swing.JPanel;

import com.wsp.DBHandler;

public class GroupsView extends JPanel{
	JPanel groupsViewPanel;
	DBHandler dbh;
	GroupsView(){
		dbh = new DBHandler();
		groupsViewPanel = new JPanel();
		groupsViewPanel.setVisible(true);
		groupsViewPanel.setBounds(400, 100, 800, 700);
		groupsViewPanel.setPreferredSize(new Dimension(500,500));
		initComponents();
		Frame.mainFrame.add(groupsViewPanel);
		
		
		showButton.addActionListener(new ActionListener() {
			 
	        public void actionPerformed(ActionEvent e)
	        {
	        	ArrayList<String> usersListt = new ArrayList<String>();
	            String selectedGroup = groupsList.getSelectedValue().toString();
	            System.out.println(selectedGroup);
	            usersListt = (ArrayList<String>) dbh.querryUsersInGroup(selectedGroup);
	            String [] userssArray = new String[usersListt.size()];
	            usersListt.toArray(userssArray);
	            usersList.setModel(new javax.swing.AbstractListModel() {
	                String[] strings = userssArray;
	                public int getSize() { return strings.length; }
	                public Object getElementAt(int i) { return strings[i]; }
	            });
	        	
	        
	        }
	    }); 
	}
	
	
	private void initComponents() {
		 
        jScrollPane1 = new javax.swing.JScrollPane();
        groupsList = new javax.swing.JList();
        jScrollPane2 = new javax.swing.JScrollPane();
        usersList = new javax.swing.JList();
        showButton = new javax.swing.JButton();
        groupsLabel = new javax.swing.JLabel();
        usersLabel = new javax.swing.JLabel();
        showUsersLabel = new javax.swing.JLabel();
        
        ArrayList<String> groupsListt = new ArrayList<String>();
        groupsListt = (ArrayList<String>) dbh.querryAllGroups();
        String [] groupsArray = new String[groupsListt.size()];
        groupsListt.toArray(groupsArray);
        groupsList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = groupsArray;
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane1.setViewportView(groupsList);
        
        
        
        usersList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { " ", " ", " ", " ", " " };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(usersList);
 
        showButton.setText("Show");
 
        groupsLabel.setText("Groups");
 
        usersLabel.setText("Users");
 
        showUsersLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        showUsersLabel.setText("Show Users");
 
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(groupsViewPanel);
        groupsViewPanel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(showButton)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(showUsersLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(1, 1, 1)
                                .addComponent(groupsLabel))
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(usersLabel))))
                .addContainerGap(128, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(showUsersLabel)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(groupsLabel)
                    .addComponent(usersLabel))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(showButton)
                .addContainerGap(58, Short.MAX_VALUE))
        );
    }// </editor-fold>                        
 
 
    // Variables declaration - do not modify                    
    private javax.swing.JLabel groupsLabel;
    private javax.swing.JList groupsList;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton showButton;
    private javax.swing.JLabel showUsersLabel;
    private javax.swing.JLabel usersLabel;
    private javax.swing.JList usersList;
    // End of variables declaration              
}
