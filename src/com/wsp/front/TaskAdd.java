package com.wsp.front;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JPanel;

import com.wsp.DBHandler;

public class TaskAdd extends JPanel {
	JPanel taskAddPanel;
	DBHandler dbh;
	  private javax.swing.JLabel TaskDeadlineLabel;
	    private javax.swing.JTextField TaskName;
	    private javax.swing.JLabel TaskNameLabel;
	    private javax.swing.JLabel TaskNumberLabel;
	    private javax.swing.JButton addTask;
	    private javax.swing.JLabel addTaskLabel;
	    private javax.swing.JComboBox assignBox;
	    private javax.swing.JLabel assignTask;
	    private javax.swing.JComboBox statusBox;
	    private javax.swing.JLabel statusTask;
	    private javax.swing.JTextField taskDeadline;
	    private javax.swing.JTextField taskDesc;
	    private javax.swing.JLabel taskDescLabel;
	    private javax.swing.JTextField taskNumber;
	
	
	TaskAdd(){
		dbh = new DBHandler();
		taskAddPanel = new JPanel();
		taskAddPanel.setVisible(true);
		taskAddPanel.setBounds(400, 100, 500, 500);
		taskAddPanel.setPreferredSize(new Dimension(500, 500));
		//taskAddPanel.setBackground(Color.green);
	      initComponents();
		
	      
	      addTask.addActionListener(new ActionListener() {
	 		 
	          public void actionPerformed(ActionEvent e)
	          {
	        	  //int taskNumber = Integer.parseInt(taskNumber.getText());
	        	  dbh.createTask( Integer.parseInt(taskNumber.getText()), TaskName.getText(), taskDeadline.getText(), statusBox.getSelectedItem().toString(), assignBox.getSelectedItem().toString(), taskDesc.getText());
	          }
	      }); 
	      
	      
	      
    	Frame.mainFrame.add(taskAddPanel);
    }// </editor-fold>    
	
 
	private void initComponents() {
		 
        TaskNumberLabel = new javax.swing.JLabel();
        TaskNameLabel = new javax.swing.JLabel();
        taskNumber = new javax.swing.JTextField();
        TaskName = new javax.swing.JTextField();
        TaskDeadlineLabel = new javax.swing.JLabel();
        taskDeadline = new javax.swing.JTextField();
        statusTask = new javax.swing.JLabel();
        statusBox = new javax.swing.JComboBox();
        assignTask = new javax.swing.JLabel();
        assignBox = new javax.swing.JComboBox();
        taskDescLabel = new javax.swing.JLabel();
        taskDesc = new javax.swing.JTextField();
        addTaskLabel = new javax.swing.JLabel();
        addTask = new javax.swing.JButton();
 
        TaskNumberLabel.setText("Task Number");
 
        TaskNameLabel.setText("Task Name");
 
        TaskDeadlineLabel.setText("Task Deadline");
 
        statusTask.setText("Task STatus");
 
        statusBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Low", "Medium", "High"}));
 
        assignTask.setText("Assigned to");
 
        DefaultComboBoxModel listModel = new DefaultComboBoxModel();
        ArrayList <String> allGroups = (ArrayList<String>) dbh.querryAllGroups();
        for(String item : allGroups){
        	listModel.addElement(item);
        }
        assignBox.setModel(listModel);
        
        
        taskDescLabel.setText("Description");
 
        addTaskLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        addTaskLabel.setText("Add Task");
 
        addTask.setText("Add Task");
 
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(taskAddPanel);
        taskAddPanel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(taskDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(TaskNameLabel)
                    .addComponent(taskDescLabel)
                    .addComponent(addTaskLabel)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(TaskNumberLabel)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(taskNumber, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(TaskName, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(taskDeadline, javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(TaskDeadlineLabel, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(85, 85, 85)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(statusBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(statusTask)
                            .addComponent(assignTask)
                            .addComponent(assignBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(addTask))
                .addContainerGap(181, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(addTaskLabel)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TaskNumberLabel)
                    .addComponent(statusTask))
                .addGap(2, 2, 2)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(taskNumber, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(statusBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TaskNameLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(TaskName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(TaskDeadlineLabel)
                    .addComponent(assignTask))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(taskDeadline, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(assignBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(taskDescLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(taskDesc, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(addTask)
                .addContainerGap(16, Short.MAX_VALUE))
        );
    }// </editor-fold>                   
	
	
	
}
