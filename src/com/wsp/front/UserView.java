package com.wsp.front;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import com.wsp.DBHandler;
import com.wsp.dbitems.Groups;

public class UserView extends JPanel {
	JPanel userViewPanel;
	DBHandler dbh;
	
	UserView(){
		dbh = new DBHandler();
		userViewPanel = new JPanel();
		userViewPanel.setVisible(true);
		userViewPanel.setBounds(400, 150, 800, 700);
		userViewPanel.setPreferredSize(new Dimension(500,500));
		initComponents();
		Frame.mainFrame.add(userViewPanel);
		
		
		
		
		showDetailButton.addActionListener(new ActionListener() {
			 
	        public void actionPerformed(ActionEvent e)
	        {
	        	
	        	ArrayList <String> allUsers = (ArrayList<String>) dbh.querryGroupsForUser(userList.getSelectedValue().toString());
	        	String [][] groupsArray = new String[allUsers.size()][2];
	        	int i=0;
	        	for(String el : allUsers){
	        		groupsArray[i][0]=el;
	        		groupsArray[i][1]=" ";
	        		i++;
	        	}
	        	detailTable.setModel(new javax.swing.table.DefaultTableModel(
	    	            groupsArray,
	    	            new String [] {
	    	                "Group", "Task"
	    	            }
	    	        ));
	        }
	    }); 
	}
	
	 private void initComponents() {
		 
	        userDetailsLabel = new javax.swing.JLabel();
	        userDetailLabel = new javax.swing.JLabel();
	        detailLabel = new javax.swing.JLabel();
	        jScrollPane1 = new javax.swing.JScrollPane();
	        userList = new javax.swing.JList();
	        jScrollPane2 = new javax.swing.JScrollPane();
	        detailTable = new javax.swing.JTable();
	        showDetailButton = new javax.swing.JButton();
	 
	        userDetailsLabel.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
	        userDetailsLabel.setText("User Detalis");
	 
	        userDetailLabel.setText("Users");
	 
	        detailLabel.setText("Details");
	 
	        
	        
	        userList.setModel(new javax.swing.AbstractListModel() {
	        	ArrayList <String> allUsers = (ArrayList<String>) dbh.querryAllUsers();
	            public int getSize() { return allUsers.size(); }
	            public Object getElementAt(int i) { return allUsers.get(i); }
	        });
	        jScrollPane1.setViewportView(userList);
	        
	        
	       // ArrayList <String> allUsers = (ArrayList<String>) dbh.querryGroupsForUser(userList.getSelectedValue().toString())
	        //String [][] groupsArray = new String[allUsers.size()][2];
	        detailTable.setModel(new javax.swing.table.DefaultTableModel(
	            new Object [][] {
	                {null, null},
	                {null, null},
	                {null, null},
	                {null, null},
	                {null, null},
	                {null, null},
	                {null, null},
	                {null, null}
	            },
	            new String [] {
	                "Group", "Task"
	            }
	        ));
	        jScrollPane2.setViewportView(detailTable);
	 
	        showDetailButton.setText("Show");
	 
	        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(userViewPanel);
	        userViewPanel.setLayout(layout);
	        layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                            .addComponent(userDetailsLabel)
	                            .addGroup(layout.createSequentialGroup()
	                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                                    .addComponent(userDetailLabel)
	                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE))
	                                .addGap(18, 18, 18)
	                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                                    .addGroup(layout.createSequentialGroup()
	                                        .addComponent(detailLabel)
	                                        .addGap(0, 0, Short.MAX_VALUE))
	                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
	                                        .addGap(0, 0, Short.MAX_VALUE)
	                                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)))))
	                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(showDetailButton)
	                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
	        );
	        layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(userDetailsLabel)
	                .addGap(18, 18, 18)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
	                    .addComponent(userDetailLabel)
	                    .addComponent(detailLabel))
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
	                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
	                    .addComponent(jScrollPane1))
	                .addGap(18, 18, 18)
	                .addComponent(showDetailButton)
	                .addContainerGap(112, Short.MAX_VALUE))
	        );
	    }// </editor-fold>                        
	 
	 
	    // Variables declaration - do not modify                    
	    private javax.swing.JLabel detailLabel;
	    private javax.swing.JTable detailTable;
	    private javax.swing.JScrollPane jScrollPane1;
	    private javax.swing.JScrollPane jScrollPane2;
	    private javax.swing.JButton showDetailButton;
	    private javax.swing.JLabel userDetailLabel;
	    private javax.swing.JLabel userDetailsLabel;
	    private javax.swing.JList userList;
	    // End of variables declaration         
}
