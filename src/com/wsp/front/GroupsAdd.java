package com.wsp.front;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.wsp.DBHandler;

public class GroupsAdd extends JPanel {
	JPanel groupAddPanel;
	DBHandler dbh;
	
	 
	 
    // Variables declaration - do not modify                    
    private javax.swing.JButton addGroup;
    private javax.swing.JLabel addGruop;
    //private javax.swing.JLabel addTasksLabel;
    //private javax.swing.JList addTastList;
    private javax.swing.JLabel addUserLabel;
    private javax.swing.JList addUserList;
    private javax.swing.JLabel gropuDescLabel;
    private javax.swing.JTextArea groupDesc;
    private javax.swing.JTextField groupName;
    private javax.swing.JLabel groupNameLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    // End of variables declaration  
	
	
	GroupsAdd(){
		dbh = new DBHandler();
		groupAddPanel = new JPanel();
		groupAddPanel.setVisible(true);
		groupAddPanel.setBounds(400, 100, 800, 700);
		groupAddPanel.setPreferredSize(new Dimension(500,500));
		initComponents() ;
		
		Frame.mainFrame.add(groupAddPanel);
		
		addGroup.addActionListener(new ActionListener() {
			 
	        public void actionPerformed(ActionEvent e)
	        {
	        	
	        	dbh.createGroup(groupName.getText(), addUserList.getSelectedValue().toString());
	        }
	    }); 
	}


    private void initComponents() {
    	 
        groupNameLabel = new javax.swing.JLabel();
        groupName = new javax.swing.JTextField();
        addUserLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        addUserList = new javax.swing.JList();
        //addTasksLabel = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        //addTastList = new javax.swing.JList();
        addGroup = new javax.swing.JButton();
        addGruop = new javax.swing.JLabel();
        gropuDescLabel = new javax.swing.JLabel();
        groupDesc = new javax.swing.JTextArea();
 
        groupNameLabel.setText("Group Name");
 
        addUserLabel.setText("Add Users");
 
        addUserList.setModel(new javax.swing.AbstractListModel() {

            ArrayList <String> allGroups = (ArrayList<String>) dbh.querryAllUsers();
            
            public int getSize() { return allGroups.size(); }
            public Object getElementAt(int i) { return allGroups.get(i); }
        });
        jScrollPane1.setViewportView(addUserList);
 
       // addTasksLabel.setText("Add Tasks");
 
       /* addTastList.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });*/
        //jScrollPane2.setViewportView(addTastList);
 
        addGroup.setText("Add Group");
 
        addGruop.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        addGruop.setText("Add Group");
 
        gropuDescLabel.setText("Group Description");
 
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(groupAddPanel);
        groupAddPanel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(addGruop)
                            .addComponent(gropuDescLabel))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(groupDesc, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(addGroup)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                        .addComponent(addUserLabel, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(groupNameLabel, javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(groupName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(59, 59, 59)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                       // .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                      // .addComponent(addTasksLabel)
                                        ))))
                        .addContainerGap(105, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addComponent(addGruop)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(groupNameLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(groupName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(16, 16, 16)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(addUserLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                    //    .addComponent(addTasksLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                      //  .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                        ))
                .addGap(18, 18, 18)
                .addComponent(gropuDescLabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(groupDesc, javax.swing.GroupLayout.DEFAULT_SIZE, 69, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(addGroup)
                .addGap(16, 16, 16))
        );
    }// </editor-fold>                        
                
}