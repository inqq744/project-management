package com.wsp.front;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import com.wsp.DBHandler;

public class Login extends JFrame{
	static String loginName;
	JFrame loginFrame;
	JButton loginButton;
	JTextField uName;
	JPasswordField pass;
	DBHandler dbh;
	Frame frame;
	public Login(){
		dbh = new DBHandler();
		loginFrame = new JFrame("Login");
		loginFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loginFrame.pack();
		
		loginFrame.setPreferredSize(new Dimension(300,200));
		loginFrame.getContentPane().setLayout(new GridLayout(3,0));
		loginFrame.setSize(300,200);
		loginFrame.setLocationRelativeTo(null);
		
		uName = new JTextField();
		pass = new JPasswordField();
		loginFrame.add(uName);
		loginFrame.add(pass);
		
		
		loginButton = new JButton("Login");
		loginFrame.add(loginButton);
		
		loginFrame.setVisible(true);
		loginButton.addActionListener(new ActionListener() {

		
	        public void actionPerformed(ActionEvent e)
	        {
	        	String uNameTF = uName.getText().toString();
	        	String passTF = pass.getText().toString();
	        	String passDB = dbh.querryPass(uNameTF);
	        	if(passTF.equals(passDB)){
	        		loginFrame.setVisible(false);
	        		frame = new Frame();
	        		loginName = uNameTF;

	        	}else{
	        		new LoginFailedFrame();
	        		uName.setText("");
	        		pass.setText("");
	        	}
	        	
	        	
	        	
	        }
	    }); 
		
	}
	
	
	
}	
