package com.wsp.front;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class LoginFailedFrame extends JFrame {
	JFrame loginFailedFrame;
	JButton tryAgain;
	JLabel loginFailed;
	LoginFailedFrame(){
		loginFailedFrame = new JFrame("Login Failed!");
		loginFailedFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		loginFailedFrame.pack();
		loginFailedFrame.setVisible(true);
		loginFailedFrame.setPreferredSize(new Dimension(300,500));
		loginFailedFrame.getContentPane().setLayout(new GridLayout(0,1));
		loginFailedFrame.setSize(300,200);
		loginFailedFrame.setLocationRelativeTo(null);
		
		loginFailed = new JLabel("Login failed!", SwingConstants.CENTER);
		loginFailedFrame.add(loginFailed);
		tryAgain = new JButton("Try Again");
		loginFailedFrame.add(tryAgain);
		
		
		tryAgain.addActionListener(new ActionListener() {

		
	        public void actionPerformed(ActionEvent e)
	        {

	        	loginFailedFrame.setVisible(false);
	        
	        	
	        }
	    }); 
		
	}
}