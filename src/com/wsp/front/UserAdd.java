package com.wsp.front;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.wsp.DBHandler;

public class UserAdd extends JPanel {
	JPanel userAddPanel;
	DBHandler dbh;
	

	  private javax.swing.JLabel NameLabel;
	    private javax.swing.JTextField userName;
	    private javax.swing.JButton addUser;
	    private javax.swing.JLabel titleAddUser;
	    private javax.swing.JLabel pass;
	    private javax.swing.JPasswordField password;
	    
	UserAdd(){
		dbh = new DBHandler();

		userAddPanel = new JPanel();
		userAddPanel.setVisible(true);
		userAddPanel.setBounds(400, 100, 800, 700);
		userAddPanel.setPreferredSize(new Dimension(500,500));
		//userAddPanel.setBackground(Color.red);
		
		//userAddPanel.setMaximumSize(userAddPanel.getPreferredSize());
		//userAddPanel.setMinimumSize(userAddPanel.getPreferredSize());
		titleAddUser = new javax.swing.JLabel();
        NameLabel = new javax.swing.JLabel();
        userName = new javax.swing.JTextField();
        pass = new javax.swing.JLabel();
        password = new javax.swing.JPasswordField();
        addUser = new javax.swing.JButton();
        
        titleAddUser.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        titleAddUser.setText("Add User");
 
        NameLabel.setText("Name");
 
        pass.setText("Password");
 
        addUser.setText("Add User");
        
		GroupLayout layout = new javax.swing.GroupLayout(userAddPanel);
		userAddPanel.setLayout(layout);
		layout.setHorizontalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(titleAddUser, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
	                        .addGap(260, 260, 260))
	                    .addGroup(layout.createSequentialGroup()
	                        .addComponent(NameLabel)
	                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
	                    .addGroup(layout.createSequentialGroup()
	                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
	                            .addComponent(userName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 112, Short.MAX_VALUE)
	                            .addComponent(pass, javax.swing.GroupLayout.Alignment.LEADING)
	                            .addComponent(addUser, javax.swing.GroupLayout.Alignment.LEADING)
	                            .addComponent(password, javax.swing.GroupLayout.Alignment.LEADING))
	                        .addGap(0, 0, Short.MAX_VALUE))))
	        );
		
		layout.setVerticalGroup(
	            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
	            .addGroup(layout.createSequentialGroup()
	                .addContainerGap()
	                .addComponent(titleAddUser)
	                .addGap(18, 18, 18)
	                .addComponent(NameLabel)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
	                .addComponent(userName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(18, 18, 18)
	                .addComponent(pass)
	                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
	                .addComponent(password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
	                .addGap(43, 43, 43)
	                .addComponent(addUser)
	                .addContainerGap(108, Short.MAX_VALUE))
	        );

		//addUser= new JButton ("Add User");
		//userAddPanel.add(addUser);
		
		//userName = new JTextField("User Name");
		//userAddPanel.add(userName);
		
		//password = new JTextField("Password");
		//userAddPanel.add(password);
		
		
		
		
		
		addUser.addActionListener(new ActionListener() {
			 
	        public void actionPerformed(ActionEvent e)
	        {
	        	dbh.createUser(userName.getText(), password.getText());
	        }
	    }); 
		
		
		Frame.mainFrame.add(userAddPanel);
		
	}

}
