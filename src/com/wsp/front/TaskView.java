package com.wsp.front;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import com.wsp.DBHandler;
import com.wsp.dbitems.Tasks;

public class TaskView extends JPanel {

	
	JPanel taskViewPanel;
	DBHandler dbh;
	
	
	TaskView(){
		dbh=new DBHandler();
		
		taskViewPanel = new JPanel();
		taskViewPanel.setVisible(true);
		taskViewPanel.setBounds(400, -200, 800, 700);
		taskViewPanel.setPreferredSize(new Dimension(500,500));
		initComponents();
		Frame.mainFrame.add(taskViewPanel);
		
		
		
		goToTaskButton.addActionListener(new ActionListener() {
			 
	        public void actionPerformed(ActionEvent e)
	        {
	        	int selectedRowIndex = taskTable.getSelectedRow();
	        	int selectedColumnIndex = taskTable.getSelectedColumn();
	        	String selectedObject = (String) taskTable.getModel().getValueAt(selectedRowIndex, 0);
	        	
	        	System.out.println(selectedObject);
	        	Menu.removeComponents();
	        	TaskDescr taskDescr = new TaskDescr(selectedObject);
	        	taskDescr.setVisible(true);
	            
	            Frame.mainFrame.pack();
	        }
	    }); 

	}
	
	
	
	
	private void initComponents() {
		 
        jScrollPane1 = new javax.swing.JScrollPane();
        taskTable = new javax.swing.JTable();
        showTaskLabe = new javax.swing.JLabel();
        goToTaskButton = new javax.swing.JButton();
        
        taskTable.setDefaultEditor(Object.class, null);
        ArrayList <Tasks> tasksList = new ArrayList<Tasks>();
        tasksList=(ArrayList<Tasks>) dbh.querryAllTasks();
        String [][] tArray = new String[tasksList.size()][tasksList.size()+1];
        int i=0, j=0;
        for(Tasks item : tasksList){
        	j=0;
        	tArray[i][j]=new Integer(item.getTaskNumber()).toString();
        	j++;
        	tArray[i][j]=item.getTaskName();
        	j++;
        	tArray[i][j]=item.getTaskDeadline();
        	j++;
        	tArray[i][j]=item.getStatus();
        	j++;
        	tArray[i][j]=item.getAssignedTo();
        	j++;
        	tArray[i][j]=item.getDescription();
        	j++;
        	i++;
        }
        taskTable.setModel(new javax.swing.table.DefaultTableModel(
            tArray,
            new String [] {
                "Number", "Name", "Deadline", "Status", "Assigned to", "Description"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
 
            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jScrollPane1.setViewportView(taskTable);
 
        showTaskLabe.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        showTaskLabe.setText("Show Task");
 
        goToTaskButton.setText("Go to task ");
 
        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(taskViewPanel);
        taskViewPanel.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 407, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(showTaskLabe)
                            .addComponent(goToTaskButton))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(showTaskLabe)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(goToTaskButton)
                .addGap(6, 6, 6))
        );
    }// </editor-fold>                        
 
 
    // Variables declaration - do not modify                    
    private javax.swing.JButton goToTaskButton;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel showTaskLabe;
    private javax.swing.JTable taskTable;
    // End of variables declaration                  
}